/**
 * 
 */
package com.ebanx.client.exceptions;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ebanx.client.exceptions.OperationException;

/**
 * @author jonhkr
 *
 */
public class OperationExceptionTest {
	private final String CODE = "CODE";
	private final String MESSAGE = "MESSAGE";
	
	@Test
	public void test() {
		try {
			throw new OperationException(CODE, MESSAGE);
		} catch(OperationException e) {
			assertEquals(e.getCode(), CODE);
			assertEquals(e.getMessage(), MESSAGE);
		}
	}

}
