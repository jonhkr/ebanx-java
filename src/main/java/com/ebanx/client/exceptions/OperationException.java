package com.ebanx.client.exceptions;

/**
 * This exception is intended to be thrown when
 * an error is returned as response of any operation.
 * 
 * @see <a href="https://developers.ebanx.com/api-reference/error-codes">https://developers.ebanx.com/api-reference/error-codes</a>
 * 
 * @author Jonas Trevisan
 */
public class OperationException extends Exception {
	private static final long serialVersionUID = 3757721342803194835L;
	
	protected String code, message;
	
	/**
	 * @param code
	 * @param message
	 */
	public OperationException(String code, String message) {
		this.setCode(code);
		this.setMessage(message);
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
