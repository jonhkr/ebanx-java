package com.ebanx.client.models;

public class RequestOperation extends Operation {
	
//	Length: 1-100	required	Customer name.
	protected String name;
//	Format: email	required	Customer email address.
	protected String email;
//	Format: ISO 4217 three letter code	required	Three-letter code of the payment currency. Supported currencies:
//	BRL
//	PEN
//	USD
	protected String currencyCode;
//	The amount in the specified currency (currency_code). E.g.: 100.50
	protected Number amount;
//	The payment hash Merchant Payment Code (merchant unique ID).
	protected String merchantPaymentCode;
//	The order number, optional identifier set by the merchant. You can have multiple payments with the same order number.
	protected String orderNumber;
	
	public RequestOperation() {
		// TODO Auto-generated constructor stub
	}

}